<meta charset="UTF-8">
    <title>| Portal Tomza |</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="assets/img/fla.png"/>
   
    <link type="text/css" rel="stylesheet" href="css/app.css"/>
    
    <link rel="stylesheet" href="assets/vendors/swiper/css/swiper.min.css">
    <link href="assets/vendors/nvd3/css/nv.d3.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/vendors/lcswitch/css/lc_switch.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">

    <link href="assets/css/custom_css/dashboard1.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/custom_css/dashboard1_timeline.css" rel="stylesheet"/>